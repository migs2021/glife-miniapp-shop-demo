const app = getApp();
import SORT_BY_OPTIONS from "/constants/const-sort-by-options";
import throwError from "/utilities/error-util";

Page({
  timeout: 0,
  data: {
    productList: [],
    cartList: [],
    addedToCartNotifDisplayed: false,
    showSpinner: false,
    page: 1,
    totalPages: 1,
    categoryList: [],
    search: "",
    category: "",
    sortBy: SORT_BY_OPTIONS[0],
    variations: null,
    productToView: null,
    loadingProductList: [...Array(4).keys()],
    isProductLoadingFailed: false,
    noProductFound: false
  },

  onLoad(query) {
    my.hideTabBar();
    this.setData({
      category: query.category ? query.category : ""
    });

    app
      .onLoadWcCurrentCurrency()
      .then(() => {
        this.loadCategoryList();
        this.loadProductList();
        app.onLoadCartList().then(res => {
          if (res) {
            app.cartList = res;
            this.setData({ cartList: res });
          }
        });
      })
      .catch(error => {
        this.setData({ isProductLoadingFailed: true });
        throwError("Error in loading Current Currency", error);
      });
  },

  onShow() {
    // Initialize all modals and popups as closed
    this.setData({
      productSelectionModalOpened: false,
      cartList: app.cartList
    });
  },

  // Search handlers
  handleInput(value) {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (value.length >= 3 || value.length === 0) {
        this.setData({
          search: value,
          productList: [],
          noProductFound: false
        });
        this.loadProductList().then(res => {
          if (res.length === 0) {
            this.setData({ noProductFound: true });
          }
        });
      }
    }, 1000);
  },
  handleClear(value) {
    this.setData({
      search: value,
      productList: [],
      noProductFound: false
    });
    this.loadProductList();
  },
  handleSubmit(value) {
    this.setData({
      search: value,
      productList: [],
      noProductFound: false
    });
    this.loadProductList();
  },

  //Product Item handlers
  onAddItemToCart(dataValue) {
    const productToView = dataValue.target.dataset.value;
    const params = {
      per_page: 50
    };
    this.setData({
      productSelectionModalOpened: true
    });
    if (productToView.type === "variable") {
      app
        .requestWcAPI(
          `/wp-json/wc/v3/products/${productToView.id}/variations`,
          "GET",
          params
        )
        .then(res => {
          const { data: variations } = res;
          this.setData({
            productToView: productToView,
            variations
          });
        });
    } else {
      this.setData({
        productToView: productToView
      });
    }
  },

  // Product Card Handlers
  onViewProductDetails(dataValue) {
    const productToView = dataValue.target.dataset.value;
    my.navigateTo({
      url: "/pages/product-details/product-details?id=" + productToView.id
    });
  },

  // Product Details Handlers
  onProdDetailsAddToCart(dataValue, quantity = 1, variation = {}) {
    app.onAddToCartFn(dataValue, quantity, variation).then(cartList => {
      app.cartList = cartList;
      this.setData({ cartList });
      // Show Added to Cart toast
      my.showToast({
        type: "success",
        content: "Added to cart",
        duration: 1000
      });
    });
  },

  onProdDetailsBuyNow(e) {
    const {
      product,
      quantity,
      variation,
      variationId,
      price
    } = e;
    return new Promise((resolve, reject) => {
      app.setUserInfo().then(
        () => {
          app.setWcUserInfo().then(
            wcUserInfo => {
              const params = {
                productId: product.id,
                quantity,
                price,
                variationId: variationId ? variationId : "",
                ...variation
              };
              const paramString = app.onSerializeQuery(params);
              if (wcUserInfo.shipping.country) {
                my.navigateTo({
                  url: `/pages/checkout/checkout?${paramString}`
                });
              } else {
                my.navigateTo({
                  url: `/pages/shipping-form/shipping-form?${paramString}`
                });
              }
              resolve();
            },
            () => {
              my.showToast({
                type: "fail",
                content: "Please try again",
                duration: 1000
              });
              reject();
            }
          );
        },
        () => {
          my.showToast({
            type: "fail",
            content: "Please try again",
            duration: 1000
          });
          reject();
        }
      );
    });
  },

  onProdDetailsModalClose() {
    this.setData({
      productToView: null,
      productSelectionModalOpened: false,
      variations: null
    });
  },

  loadCategoryList() {
    return new Promise(resolve => {
      app
        .requestWcAPI("/wp-json/wc/v3/products/categories", "GET", {
          hide_empty: true
        })
        .then(res => {
          const { data } = res;
          const all = [{ id: "", name: "All" }];
          this.setData({
            categoryList: [...all, ...data]
          });
          resolve([...all, ...data]);
        });
    });
  },

  // List loading handlers
  loadProductList(page = 1) {
    return new Promise(resolve => {
      const params = {
        page,
        per_page: 4,
        search: this.data.search,
        category: this.data.category,
        status: "publish",
        ...this.data.sortBy.value
      };
      app.requestWcAPI("/wp-json/wc/v3/products", "GET", params).then(res => {
        const { data, headers } = res;
        resolve(data);
        this.setData({
          productList: page > 1 ? [...this.data.productList, ...data] : data,
          page: page,
          showSpinner: false,
          totalPages: headers["x-wp-totalpages"]
            ? headers["x-wp-totalpages"]
            : headers["X-WP-TotalPages"][0]
        });
      });
    });
  },

  scrollToLower() {
    const { page, showSpinner } = this.data;
    const newPage = page + 1;
    if (!showSpinner && newPage <= this.data.totalPages) {
      this.setData({ showSpinner: true });
      this.loadProductList(newPage);
    }
  },

  // Sort By Picker Handler
  onPickerTap() {
    my.showActionSheet({
      title: "Sort by:",
      items: SORT_BY_OPTIONS.map(option => option.textDisplay),
      cancelButtonText: "Cancel",
      destructiveBtnIndex: SORT_BY_OPTIONS.findIndex(
        sortBy => sortBy.textDisplay === this.data.sortBy.textDisplay
      ),
      badges: [],
      success: selection => {
        this.setData({
          sortBy: SORT_BY_OPTIONS[selection.index]
        });
        this.loadProductList();
      },
      fail: () => {},
      complete: () => {}
    });
  },
  onCategoryTap(e) {
    const category = e.target.targetDataset.value;
    this.setData({
      category,
      productList: [],
      search: "",
      noProductFound: false
    });
    this.loadProductList();
  }
});
