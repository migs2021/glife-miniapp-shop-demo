const app = getApp();

Page({
  data: {
    cartList: [],
    formattedCartTotal: "",
    isCheckoutLoading: false
  },
  onLoad() {
    my.hideTabBar();
  },
  onShow() {
    this.onSetTotal();
    this.setData({
      cartList: app.cartList
    });
  },
  // Cart item handlers
  onItemRemoveFromCart(dataValue) {
    const cartItemId = dataValue.target.targetDataset.value;
    const newCartList = [...app.cartList];
    newCartList.splice(cartItemId, 1);
    app.onSetCartList(newCartList).then(res => {
      app.cartList = res;
      this.setData({ cartList: res });
      this.onSetTotal();
    });
  },
  onCheckout() {
    this.setData({ isCheckoutLoading: true });
    app.setUserInfo().then(
      () => {
        app.setWcUserInfo().then(
          res => {
            if (res.shipping.country) {
              my.navigateTo({ url: "/pages/checkout/checkout" });
            } else {
              my.navigateTo({ url: "/pages/shipping-form/shipping-form" });
            }
            this.setData({ isCheckoutLoading: false });
          },
          () => {
            my.showToast({
              type: "fail",
              content: "Please try again",
              duration: 1000
            });
            this.setData({ isCheckoutLoading: false });
          });
      },
      () => {
        my.showToast({
          type: "fail",
          content: "Please try again",
          duration: 1000
        });
        this.setData({ isCheckoutLoading: false });
      }
    );
  },
  onSetQuantity(params) {
    const newCartList = [...app.cartList];
    newCartList[params.cartKey].line_item.quantity = params.quantity;
    app.onSetCartList(newCartList).then(res => {
      app.cartList = res;
      this.setData({ cartList: res });
      this.onSetTotal();
    });
  },
  goToProductListPage() {
    my.switchTab({ url: "/pages/product-list/product-list" });
  },
  onSetTotal() {
    const initialCartTotal = 0;
    const cartTotal = app.cartList.reduce(
      (previousValue, currentValue) =>
        previousValue +
        parseFloat(currentValue.price) * currentValue.line_item.quantity,
      initialCartTotal
    );
    this.setData({
      formattedCartTotal: app.formatCurrency(cartTotal, app.wcCurrency.code)
    });
  }
});
