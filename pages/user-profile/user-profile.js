const app = getApp();
import addTimeout from "/utilities/timeout-util";
import throwError from "/utilities/error-util";

Page({
  data: {
    doneLoadingProfile: false,
    doneLoadingOrders: false,
    userInfo: null,
    wcUserInfo: null,
    cartList: [],
    orderList: [],
    loadingOrderList: [...Array(2).keys()],
    hasErrorLoadingOrder: false,
    hasErrorSettingUserInfo: false,
    hasErrorWcUserInfo: false
  },
  onLoad() {
    my.hideTabBar();
    app.setUserInfo()
      .then(userInfo => {
        this.setData({ userInfo });
        this.setData({ doneLoadingProfile: true });
        app
          .setWcUserInfo()
          .then(wcUserInfo => {
            this.setData({ wcUserInfo });
            this.onLoadOrders();
          })
          .catch(error => {
            this.setData({
              hasErrorWcUserInfo: true
            });
            this.markOrderLoadingCompleted();
            throwError("Error in getting wcUserInfo", error);
          });
      })
      .catch(error => {
        this.setData({
          hasErrorSettingUserInfo: true
        });
        throwError("Error in setting user info", error);
      });
  },
  onShow() {
    this.setData({
      cartList: app.cartList
    });
    this.onLoadOrders();
  },
  markOrderLoadingCompleted() {
    this.setData({ doneLoadingOrders: true });
  },
  onLoadOrders() {
    const { wcUserInfo } = this.data;
    if (wcUserInfo) {
      const body = {
        customer: wcUserInfo.id
      };
      app
        .requestWcAPI("/wp-json/wc/v3/orders", "GET", body)
        .then(res => {
          const { data } = res;
          data.forEach((order, orderIndex) => {
            order.line_items.forEach((lineItem, lineItemsIndex) => {
              const variations = [];
              lineItem.meta_data.forEach(metaData => {
                if (
                  !variations.find(
                    variation => variation.attribute === metaData.display_key
                  ) &&
                  metaData.display_value
                ) {
                  variations.push({
                    attribute: metaData.display_key,
                    value: metaData.display_value
                  });
                }
              });
              data[orderIndex].line_items[
                lineItemsIndex
              ].variation = variations;
            });
          });
          this.setData({ orderList: data });
          this.markOrderLoadingCompleted();
        })
        .catch(error => {
          this.setData({
            hasErrorLoadingOrder: true
          });
          this.markOrderLoadingCompleted();
          throwError("Error in getting orders", error);
        });
    }
  }
});
