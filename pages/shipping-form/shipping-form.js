const app = getApp();

Page({
  data: {
    countries: null,
    formData: {
      first_name: "",
      last_name: "",
      country: 0,
      address_1: "",
      address_2: "",
      city: "",
      state: 0,
      postcode: ""
    },
    requiredField: [
      "first_name",
      "last_name",
      "country",
      "address_1",
      "city",
      "postcode"
    ],
    isSubmitDisabled: true,
    userInfo: null,
    wcUserInfo: null,
    query: {},
    isContinueLoading: false
  },
  onLoad(query) {
    this.setData({ query });
    app.setUserInfo().then(userInfo => {
      this.setData({ userInfo });
      app.setWcUserInfo().then(wcUserInfo => {
        this.setData({ wcUserInfo });
        app.onLoadCountries().then(countries => {
          const countryIndex = wcUserInfo.billing.country
            ? countries.findIndex(
                country => country.code === wcUserInfo.billing.country
              )
            : 0;
          const stateIndex = wcUserInfo.billing.state
            ? countries[countryIndex].states.findIndex(
                state => state.code === wcUserInfo.billing.state
              )
            : 0;
          this.setData({
            countries,
            formData: {
              first_name: wcUserInfo.billing.first_name
                ? wcUserInfo.billing.first_name
                : wcUserInfo.first_name,
              last_name: wcUserInfo.billing.last_name
                ? wcUserInfo.billing.last_name
                : wcUserInfo.last_name,
              country: countryIndex,
              address_1: wcUserInfo.billing.address_1
                ? wcUserInfo.billing.address_1
                : "",
              address_2: wcUserInfo.billing.address_2
                ? wcUserInfo.billing.address_2
                : "",
              city: wcUserInfo.billing.city ? wcUserInfo.billing.city : "",
              state: stateIndex,
              postcode: wcUserInfo.billing.postcode
                ? wcUserInfo.billing.postcode
                : ""
            }
          });
          this.isDisabled();
        });
      });
    });
  },
  onFormSubmit(e) {
    this.setData({ isContinueLoading: true });
    const {
      isSubmitDisabled,
      countries,
      userInfo,
      wcUserInfo,
      query
    } = this.data;
    if (!isSubmitDisabled) {
      const newFormData = { ...e.detail.value };
      newFormData.state =
        countries[newFormData.country].states.length > 0
          ? countries[newFormData.country].states[newFormData.state].code
          : "";
      newFormData.country = countries[newFormData.country].code;
      const body = {
        billing: {
          ...newFormData,
          email: wcUserInfo.email,
          phone: userInfo.contactNo,
          company: ""
        },
        shipping: {
          ...newFormData,
          phone: userInfo.contactNo,
          company: ""
        },
        meta_data: [{ key: "wc_address_book_shipping", value: ["shipping"] }]
      };
      const paramString = app.onSerializeQuery(body);
      app
        .requestWcAPI(`/wp-json/wc/v3/customers/${wcUserInfo.id}?${paramString}`, "PUT")
        .then(
          res => {
            const { data } = res;
            app.wcUserInfo = data;
            const paramString = app.onSerializeQuery(query);
            const url =
              "/pages/checkout/checkout" +
              (paramString ? `?${paramString}` : "");
            my.redirectTo({ url: url });
            this.setData({ isContinueLoading: false });
          },
          () => {
            this.setData({ isContinueLoading: false });
          }
        );
    }
  },
  onPickerChange(e) {
    const { target, detail } = e;
    const { formData } = this.data;
    const newFormData = { ...formData };
    newFormData[target.dataset.name] = detail.value;
    if (target.dataset.name === "country") {
      newFormData.state = 0;
    }
    this.setData({ formData: newFormData });
    this.isDisabled();
  },
  onInput(e) {
    const { target, detail } = e;
    const { formData } = this.data;
    const newFormData = { ...formData };
    newFormData[target.dataset.name] = detail.value;
    this.setData({ formData: newFormData });
    this.isDisabled();
  },
  isDisabled() {
    const { formData, requiredField } = this.data;
    const isSubmitDisabled = Object.entries(formData).every(([key, value]) => {
      const isIncluded = requiredField.includes(key);
      return !isIncluded || (isIncluded && value.toString() !== "");
    });
    this.setData({ isSubmitDisabled: !isSubmitDisabled });
  }
});
