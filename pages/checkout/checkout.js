const app = getApp();

Page({
  data: {
    checkoutList: [],
    cartList: [],
    checkoutTotal: 0,
    formattedCheckoutTotal: "",
    lineItems: [],
    orderId: null,
    isBuyNow: false,
    paymentId: null,
    isPayNowLoading: false,
    loadingCheckoutList: [...Array(1).keys()],
    wcUserInfo: null,
    displayedShippingAddress: "",
    query: {}
  },
  onLoad(query) {
    this.setData({ query });
    let checkoutTotal = 0;
    const lineItems = [];
    const checkoutList = [];
    if (query.productId) {
      app
        .requestWcAPI("/wp-json/wc/v3/products/" + query.productId)
        .then(res => {
          const { data } = res;
          const variation = data.attributes.map(attribute => ({
            key: attribute.name,
            value: attribute.options[query[attribute.name]]
          }));
          checkoutTotal = parseFloat(query.price) * query.quantity;
          const checkoutItem = {
            id: query.variationId ? query.variationId : data.id,
            images: data.images[0].src,
            name: data.name,
            quantity: query.quantity,
            price: checkoutTotal,
            variation: variation
          };
          checkoutList.push(checkoutItem);
          const metaData = data.attributes.map(attribute => ({
            key: attribute.name,
            value: attribute.options[query[attribute.name]]
          }));
          lineItems.push({
            product_id: query.variationId ? query.variationId : data.id,
            quantity: query.quantity,
            meta_data: metaData
          });
          this.setData({
            cartList: app.cartList,
            checkoutTotal,
            formattedCheckoutTotal: app.formatCurrency(
              checkoutTotal,
              app.wcCurrency.code
            ),
            lineItems,
            checkoutList,
            isBuyNow: true
          });
        });
    } else {
      app.cartList.forEach(cartItem => {
        const checkoutItem = {
          id: cartItem.id,
          images: cartItem.image ? cartItem.image.src : cartItem.images[0].src,
          name: cartItem.name,
          quantity: cartItem.line_item.quantity,
          price: parseFloat(cartItem.price) * cartItem.line_item.quantity,
          variation: cartItem.line_item.meta_data
        };
        checkoutList.push(checkoutItem);
        checkoutTotal += checkoutItem.price;
        lineItems.push(cartItem.line_item);
      });
      this.setData({
        cartList: app.cartList,
        checkoutTotal,
        formattedCheckoutTotal: app.formatCurrency(
          checkoutTotal,
          app.wcCurrency.code
        ),
        lineItems,
        checkoutList
      });
    }
  },
  onShow() {
    const {
      address_1,
      address_2,
      city,
      state,
      country
    } = app.wcUserInfo.shipping;
    const displayedShippingAddress = [
      [address_1, address_2].join(" ").trim(),
      city,
      ...(state ? [state] : []),
      country
    ].join(", ");
    this.setData({ wcUserInfo: app.wcUserInfo, displayedShippingAddress });
  },

  onPayNow() {
    this.setData({ isPayNowLoading: true });
    const { wcUserInfo } = this.data;
    const body = {
      payment_method: "bacs",
      payment_method_title: "GCash",
      set_paid: false,
      billing: wcUserInfo.billing,
      shipping: wcUserInfo.shipping,
      customer_id: wcUserInfo.id,
      line_items: this.data.lineItems
    };
    app.requestWcAPI("/wp-json/wc/v3/orders", "POST", body).then(res => {
      const { data } = res;
      this.setData({ orderId: data.id });
      const paymentAmount = data.total * 100;
      const body = {
        paymentRequestId: data.order_key,
        paymentOrderTitle: `Order #${data.id}`,
        paymentAmount: {
          currency: data.currency,
          value: paymentAmount.toString()
        }
      };
      this.pay(body);
    });
  },

  startPaymentTransaction(paymentUrl) {
    const body = {
      paymentUrl
    };
    app.startPaymentTransaction(body).then(res => {
      if (res.resultCode === "9000") {
        const body = {
          set_paid: true,
          transaction_id: this.data.paymentId
        };
        const paramString = app.onSerializeQuery(body);
        app
          .requestWcAPI(
            `/wp-json/wc/v3/orders/${this.data.orderId}?${paramString}`,
            "PUT"
          )
          .then(() => {
            if (!this.data.isBuyNow) {
              app.onSetCartList([]);
            }
            my.reLaunch({
              url: "/pages/product-list/product-list"
            });
          });
      } else if (res.resultCode === "6001") {
        this.setData({ isPayNowLoading: false });
      }
    });
  },
  pay(merchantBody) {
    const restfulPath = "/v1/payments/pay.htm";
    const body = {
      partnerId: app.merchantId,
      appId: app.appId,
      productCode: "51090000001432700001",
      paymentFactor: { isCashierPayment: true },
      extendInfo: '{"customerBelongsTo":"GCASH"}',
      ...merchantBody
    };
    app.requestOpenAPI(restfulPath, body).then(res => {
      const paymentId = res.paymentId ? res.paymentId : null;
      const paymentUrl =
        res.actionForm && res.actionForm.redirectionUrl
          ? res.actionForm.redirectionUrl
          : null;
      this.setData({ paymentId });
      if (!paymentId) {
        my.showToast({
          type: "fail",
          content: "Please try again",
          duration: 1000
        });
        this.setData({ isPayNowLoading: false });
      }
      if (paymentUrl) {
        this.startPaymentTransaction(paymentUrl);
      }
    });
  },
  inquiryPayment() {
    const restfulPath = "/v1/payments/inquiryPayment.htm";
    const body = {
      partnerId: app.merchantId,
      paymentId: this.data.paymentId
    };
    app.requestOpenAPI(restfulPath, body).then(res => {
      const paymentRequestId = res.paymentRequestId
        ? res.paymentRequestId
        : null;
    });
  },
  onShippingAddressCardClick() {
    const { query } = this.data;
    const paramString = app.onSerializeQuery(query);
    const url = "/pages/shipping-form/shipping-form" + (paramString ? `?${paramString}` : "");
    my.navigateTo({ url: url });
  },
  onContactCardClick() {
    const { query } = this.data;
    const paramString = app.onSerializeQuery(query);
    const url =
      "/pages/contact-form/contact-form" +
      (paramString ? `?${paramString}` : "");
    my.navigateTo({ url: url });
  }
});
