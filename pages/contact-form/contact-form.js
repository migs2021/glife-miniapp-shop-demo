const app = getApp();

Page({
  data: {
    formData: {
      email: "",
      phone: ""
    },
    requiredField: ["email", "phone"],
    isSubmitDisabled: true,
    wcUserInfo: null,
    query: {},
    isContinueLoading: false
  },
  onLoad(query) {
    this.setData({ query });
    app.setWcUserInfo().then(wcUserInfo => {
      this.setData({
        wcUserInfo,
        formData: {
          email: wcUserInfo.billing.email,
          phone: wcUserInfo.billing.phone
        }
      });
      this.isDisabled();
    });
  },
  onFormSubmit(e) {
    this.setData({ isContinueLoading: true });
    const { isSubmitDisabled, wcUserInfo, query } = this.data;
    if (!isSubmitDisabled) {
      const newFormData = { ...e.detail.value };
      const body = {
        billing: newFormData,
        shipping: { phone: newFormData.phone }
      };
      const paramString = app.onSerializeQuery(body);
      app
        .requestWcAPI(`/wp-json/wc/v3/customers/${wcUserInfo.id}?${paramString}`, "PUT")
        .then(
          res => {
            const { data } = res;
            app.wcUserInfo = data;
            const paramString = app.onSerializeQuery(query);
            const url =
              "/pages/checkout/checkout" +
              (paramString ? `?${paramString}` : "");
            my.redirectTo({ url: url });
            this.setData({ isContinueLoading: false });
          },
          () => {
            this.setData({ isContinueLoading: false });
          }
        );
    }
  },
  onPickerChange(e) {
    const { target, detail } = e;
    const { formData } = this.data;
    const newFormData = { ...formData };
    newFormData[target.dataset.name] = detail.value;
    if (target.dataset.name === "country") {
      newFormData.state = 0;
    }
    this.setData({ formData: newFormData });
    this.isDisabled();
  },
  onInput(e) {
    const { target, detail } = e;
    const { formData } = this.data;
    const newFormData = { ...formData };
    newFormData[target.dataset.name] = detail.value;
    this.setData({ formData: newFormData });
    this.isDisabled();
  },
  isDisabled() {
    const { formData, requiredField } = this.data;
    const isSubmitDisabled = Object.entries(formData).every(([key, value]) => {
      const isIncluded = requiredField.includes(key);
      return !isIncluded || (isIncluded && value.toString() !== "");
    });
    this.setData({ isSubmitDisabled: !isSubmitDisabled });
  }
});
