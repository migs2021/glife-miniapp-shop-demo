const app = getApp();

Page({
  data: {
    product: null,
    variation: {},
    quantity: 1,
    cartList: [],
    price: 0,
    formattedPrice: "",
    cartItem: null,
    variations: null,
    selectedVariation: null,
    categories: "",
    cartKey: null,
    isBuyNowLoading: false
  },
  onLoad(query) {
    const cartItem = query.cartKey ? app.cartList[query.cartKey] : null;
    this.setData({
      cartItem,
      quantity: cartItem ? cartItem.line_item.quantity : this.data.quantity
    });
    app.requestWcAPI("/wp-json/wc/v3/products/" + query.id).then(res => {
      const { data } = res;
      if (data.type === "variation") {
        app
          .requestWcAPI("/wp-json/wc/v3/products/" + data.parent_id)
          .then(res => {
            const { data: product } = res;
            const variation = {};
            const categories = product.categories
              .map(category => category.name)
              .join(",");
            product.attributes.forEach(attribute => {
              const attributeValue = cartItem.line_item.meta_data.find(
                variation => variation.key === attribute.name
              ).value;
              variation[attribute.name] = attribute.options.findIndex(
                option => option === attributeValue
              );
            });
            const params = {
              per_page: 50
            };
            app
              .requestWcAPI(
                `/wp-json/wc/v3/products/${product.id}/variations`,
                "GET",
                params
              )
              .then(res => {
                const { data } = res;
                const selectedVariation = data.find(variationItem => {
                  let returnValue = true;
                  variationItem.attributes.forEach(attribute => {
                    if (
                      attribute.option !==
                      product.attributes.find(
                        attr => attr.name === attribute.name
                      ).options[variation[attribute.name]]
                    ) {
                      returnValue = false;
                      return;
                    }
                  });
                  return returnValue;
                });
                const formattedPrice = app.formatCurrency(
                  selectedVariation.price,
                  app.wcCurrency.code
                );
                this.setData({
                  product,
                  variation,
                  price: selectedVariation.price,
                  formattedPrice,
                  variations: data,
                  selectedVariation,
                  categories,
                  cartKey: query.cartKey ? query.cartKey : null
                });
              });
          });
      } else if (data.type === "variable") {
        const product = data;
        const categories = product.categories
          .map(category => category.name)
          .join(",");
        const variation = {};
        product.attributes.forEach(attribute => {
          variation[attribute.name] = 0;
        });
        const params = {
          per_page: 50
        };
        app
          .requestWcAPI(
            `/wp-json/wc/v3/products/${product.id}/variations`,
            "GET",
            params
          )
          .then(res => {
            const { data } = res;
            const selectedVariation = data.find(variationItem => {
              let returnValue = true;
              variationItem.attributes.forEach(attribute => {
                if (
                  attribute.option !==
                  product.attributes.find(attr => attr.name === attribute.name)
                    .options[variation[attribute.name]]
                ) {
                  returnValue = false;
                  return;
                }
              });
              return returnValue;
            });
            const formattedPrice = app.formatCurrency(
              selectedVariation.price,
              app.wcCurrency.code
            );
            this.setData({
              product,
              variation,
              price: selectedVariation.price,
              formattedPrice,
              variations: data,
              selectedVariation,
              categories
            });
          });
      } else {
        const formattedPrice = app.formatCurrency(
          data.price,
          app.wcCurrency.code
        );
        const categories = data.categories
          .map(category => category.name)
          .join(",");
        this.setData({
          product: data,
          price: data.price,
          formattedPrice,
          categories
        });
      }
    });
  },
  onShow() {
    this.setData({
      cartList: app.cartList
    });
  },

  onAddToCart() {
    const { variation, quantity, product, selectedVariation, cartKey } = this.data;
    const params = {
      product,
      variation: selectedVariation,
      cartKey
    };
    app.onAddToCartFn(params, quantity, variation).then(cartList => {
      app.cartList = cartList;
      this.setData({ cartList });
      // Show Added to Cart toast
      my.showToast({
        type: "success",
        content: "Added to cart",
        duration: 1000
      });
    });
  },
  onBuyNow() {
    this.setData({ isBuyNowLoading: true });
    app.setUserInfo().then(
      () => {
        app.setWcUserInfo().then(
          wcUserInfo => {
            const {
              selectedVariation,
              quantity,
              variation,
              product,
              price
            } = this.data;
            const params = {
              productId: product.id,
              quantity,
              price,
              variationId: selectedVariation ? selectedVariation.id : "",
              ...variation
            };
            const paramString = app.onSerializeQuery(params);
            if (wcUserInfo.shipping.country) {
              my.navigateTo({ url: `/pages/checkout/checkout?${paramString}` });
            } else {
              my.navigateTo({
                url: `/pages/shipping-form/shipping-form?${paramString}`
              });
            }
            this.setData({ isBuyNowLoading: false });
          },
          () => {
            my.showToast({
              type: "fail",
              content: "Please try again",
              duration: 1000
            });
            this.setData({ isBuyNowLoading: false });
          }
        );
      },
      () => {
        my.showToast({
          type: "fail",
          content: "Please try again",
          duration: 1000
        });
        this.setData({ isBuyNowLoading: false });
      }
    );
  },
  onSetVariation(variation) {
    const newVariation = { ...this.data.variation, ...variation };
    const { product, variations } = this.data;
    if (product.variations.length > 1) {
      const selectedVariation = variations.find(variationItem => {
        let returnValue = true;
        variationItem.attributes.forEach(attribute => {
          if (
            attribute.option !==
            product.attributes.find(attr => attr.name === attribute.name)
              .options[newVariation[attribute.name]]
          ) {
            returnValue = false;
            return;
          }
        });
        return returnValue;
      });
      const formattedPrice = app.formatCurrency(
        selectedVariation.price,
        app.wcCurrency.code
      );
      this.setData({
        formattedPrice,
        selectedVariation
      });
    }
    this.setData({
      variation: newVariation
    });
  },

  onSetQuantity(quantity) {
    this.setData({
      quantity
    });
  }
});
