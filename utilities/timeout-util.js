// Add timeout to Promises
const addTimeout = (promise, timeOut) => {
  const ERROR_TIMEOUT = "Request timeout";
  const DEFAULT_TIMEOUT = 10000; /*10 seconds*/

  let timer;
  return Promise.race([
    promise,
    new Promise(
      (resolve, reject) =>
        (timer = setTimeout(() => reject(ERROR_TIMEOUT), timeOut ? timeOut: DEFAULT_TIMEOUT))
    )
  ]).finally(() => {
    clearTimeout(timer);
  });
};

export default addTimeout;
