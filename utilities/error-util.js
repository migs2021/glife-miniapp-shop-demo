const throwError = (errorMessage, origError ) => {
  // Do not delete - for printing error
  console.error({
    errorMessage,
    origError
  });
  throw new Error(errorMessage);
};

export default throwError;