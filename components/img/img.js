const app = getApp();
const NO_IMAGE_PATH = "/assets/icons/no-image.png";

Component({
  mixins: [],
  data: {
    imgUrl: ""
  },
  props: {
    mode: "scaleToFill",
    className: "",
    src: ""
  },
  didMount() {
    const { src } = this.props;
    if (this.data.imgUrl === NO_IMAGE_PATH) {
      return;
    }
    if (src.includes(app.wcHost)) {
      this.setData({ imgUrl: src });
    } else {
      this.setData({ imgUrl: app.wcHost + src });
    }
  },
  didUpdate() {
    const { src } = this.props;
    if (this.data.imgUrl === NO_IMAGE_PATH) {
      return;
    }
    if (src.includes(app.wcHost)) {
      this.setData({ imgUrl: src });
    } else {
      this.setData({ imgUrl: app.wcHost + src });
    }
  },
  methods: {
    onImageError: function onImageError(e) {
      this.setData({ imgUrl: NO_IMAGE_PATH });
    }
  }
});
