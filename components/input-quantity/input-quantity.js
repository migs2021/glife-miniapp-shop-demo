const app = getApp();

Component({
  mixins: [],
  data: {
    quantity: 1,
    minQuantity: 1,
    maxQuantity: 99
  },
  props: {
    onSetQuantity: e => {},
    value: null,
    minQuantity: null,
    maxQuantity: null,
    cartKey: null,
    className: '',
    size: ''
  },
  methods: {
    variationChange: function variationChange(e) {
      const { target, detail } = e;
      const variation = this.data.variation;
      variation[target.dataset.name] = detail.value;
      this.setData({
        variation
      });
      const onSetVariation = this.props.onSetVariation;
      if (onSetVariation) {
        onSetVariation(variation);
      }
    },
    onClickAddQuantity: function onClickAddQuantity() {
      const quantity =
        (this.props.value ? this.props.value : this.data.quantity) + 1;
      const maxQuantity = this.props.maxQuantity
        ? this.props.maxQuantity
        : this.data.maxQuantity;
      if (maxQuantity >= quantity) {
        this.setData({ quantity });
        const cartKey = this.props.cartKey;
        const onSetQuantity = this.props.onSetQuantity;
        if (onSetQuantity) {
          if (cartKey !== null) {
            const params = {
              quantity,
              cartKey
            };
            onSetQuantity(params);
          } else {
            onSetQuantity(quantity);
          }
        }
      }
    },
    onClickMinusQuantity: function onClickMinusQuantity() {
      const quantity =
        (this.props.value ? this.props.value : this.data.quantity) - 1;
      const minQuantity = this.props.minQuantity
        ? this.props.minQuantity
        : this.data.minQuantity;
      if (minQuantity <= quantity) {
        this.setData({ quantity });
        const cartKey = this.props.cartKey;
        const onSetQuantity = this.props.onSetQuantity;
        if (onSetQuantity) {
          if (cartKey !== null) {
            const params = {
              quantity,
              cartKey
            };
            onSetQuantity(params);
          } else {
            onSetQuantity(quantity);
          }
        }
      }
    }
  }
});
