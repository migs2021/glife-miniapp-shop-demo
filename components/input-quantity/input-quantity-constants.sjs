const INPUT_QUANTITY_SIZES = {
  REGULAR: "regular",
  SMALL: "small"
};

export default INPUT_QUANTITY_SIZES;

