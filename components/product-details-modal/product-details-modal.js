const app = getApp();

Component({
  mixins: [],
  data: {
    variation: {},
    quantity: 1,
    price: 0,
    selectedVariation: null,
    formattedPrice: "",
    categories: "",
    isBuyNowLoading: false
  },
  props: {
    show: false,
    product: {
      name: "",
      price: "",
      description: "",
      sizes: "",
      colors: "",
      quantity: 1,
      imgUrl: "",
      images: [],
      attributes: {}
    },
    addedToCartNotifDisplayed: false,
    variations: null
  },
  didUpdate() {
    const { variation, selectedVariation, categories } = this.data;
    const { product, variations } = this.props;
    if (!product) {
      this.setData({
        variation: {},
        quantity: 1,
        categories: ""
      });
      return;
    }
    const newVariation = { ...variation };
    product.attributes.forEach(attribute => {
      newVariation[attribute.name] = variation[attribute.name]
        ? variation[attribute.name]
        : 0;
    });
    if (product.variations.length > 0 && variations) {
      const newSelectedVariation = variations.find(variationItem => {
        let returnValue = true;
        variationItem.attributes.forEach(attribute => {
          if (
            attribute.option !==
            product.attributes.find(attr => attr.name === attribute.name)
              .options[newVariation[attribute.name]]
          ) {
            returnValue = false;
            return;
          }
        });
        return returnValue;
      });
      if (
        !selectedVariation ||
        (selectedVariation && selectedVariation.id !== newSelectedVariation.id)
      ) {
        const formattedPrice = app.formatCurrency(
          newSelectedVariation.price,
          app.wcCurrency.code
        );
        this.setData({
          price: newSelectedVariation.price,
          formattedPrice,
          selectedVariation: newSelectedVariation
        });
      }
    } else {
      const formattedPrice = app.formatCurrency(
        product.price,
        app.wcCurrency.code
      );
      this.setData({
        price: product.price,
        formattedPrice
      });
    }
    if (Object.keys(variation).length === 0) {
      this.setData({
        variation: newVariation
      });
    }
    if (!categories) {
      const newCategories = product.categories
        .map(category => category.name)
        .join(",");
      this.setData({ categories: newCategories });
    }
  },
  methods: {
    onAddToCart: function onAddToCart() {
      var onAddToCart = this.props.onAddToCart;
      const { product } = this.props;
      const { selectedVariation } = this.data;
      const params = {
        product,
        variation: selectedVariation
      };
      if (onAddToCart) {
        onAddToCart(params, this.data.quantity, this.data.variation);
      }
    },

    onBuyNow: function onBuyNow() {
      this.setData({ isBuyNowLoading: true });
      var onBuyNow = this.props.onBuyNow;
      const { product } = this.props;
      const { quantity, variation, price, selectedVariation } = this.data;

      if (onBuyNow) {
        const params = {
          product,
          quantity,
          variation,
          variationId: selectedVariation ? selectedVariation.id : null,
          price
        };
        onBuyNow(params).then(
          () => {
            this.setData({ isBuyNowLoading: false });
          },
          () => {
            this.setData({ isBuyNowLoading: false });
          }
        );
      }
    },

    onModalClose: function onModalClose(e) {
      var onModalClose = this.props.onModalClose;

      if (onModalClose) {
        onModalClose(e);
      }
    },

    onModalClick: function onModalClick(e) {
      var onModalClick = this.props.onModalClick;

      if (onModalClick) {
        onModalClick(e);
      }
    },

    onSetVariation: function onSetVariation(variation) {
      const newVariation = { ...this.data.variation, ...variation };
      this.setData({
        variation: newVariation
      });
    },

    onSetQuantity: function onSetQuantity(quantity) {
      this.setData({
        quantity
      });
    }
  }
});
