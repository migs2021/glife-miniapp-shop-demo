const app = getApp();
const NO_IMAGE_PATH = "/assets/icons/no-image.png";

Component({
  mixins: [],
  data: {
    totalPrice: ""
  },
  props: {
    key: "",
    id: "",
    name: "",
    price: "",
    imgUrl: "",
    total: "",
    variations: [],
    quantity: 1,
    cartKey: "",
    onItemRemove: e => {},
    onSetQuantity: e => {},
    itemCardView: null,
    className: "",
    noImgIconPath: NO_IMAGE_PATH
  },
  deriveDataFromProps() {
    this.setData({
      imgUrl: this.props.imgUrl ? this.props.imgUrl : NO_IMAGE_PATH
    });
  },
  didMount() {
    const { total } = this.props;
    const formattedTotal = app.formatCurrency(total, app.wcCurrency.code);
    this.setData({
      totalPrice: formattedTotal
    });
  },
  didUpdate() {
    const { total } = this.props;
    const formattedTotal = app.formatCurrency(total, app.wcCurrency.code);
    this.setData({
      totalPrice: formattedTotal
    });

    this.setData({
      imgUrl: this.props.imgUrl ? this.props.imgUrl : NO_IMAGE_PATH
    });
  },
  methods: {
    onItemRemove: function onItemRemove(e) {
      var onItemRemove = this.props.onItemRemove;

      if (onItemRemove) {
        onItemRemove(e);
      }
    },
    onSetQuantity: function onSetQuantity(e) {
      var onSetQuantity = this.props.onSetQuantity;
      if (onSetQuantity) {
        onSetQuantity(e);
      }
    },
    onImageError(e) {
      this.setData({ imgUrl: NO_IMAGE_PATH });
    }
  }
});
