Component({
  mixins: [],
  data: {
    variation: {}
  },
  props: {
    attribute: {
      name: "",
      options: []
    },
    value: {},
    onSetVariation: e => {}
  },
  methods: {
    variationChange: function variationChange(e) {
      const { target, detail } = e;
      const variation = this.data.variation;
      variation[target.dataset.name] = detail.value;
      this.setData({
        variation
      });
      const onSetVariation = this.props.onSetVariation;
      if (onSetVariation) {
        onSetVariation(variation);
      }
    }
  }
});
