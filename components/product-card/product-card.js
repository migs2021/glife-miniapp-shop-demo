const app = getApp();
const NO_IMAGE_PATH = "/assets/icons/no-image.png";

Component({
  mixins: [],
  data: {
    formattedPrice: "",
    category: ""
  },
  props: {
    product: {
      name: "",
      price: "",
      category: "",
      imgUrl: ""
    },
    onAddItemToCart: e => {},
    buyNow: e => {}
  },
  didMount() {
    const { product } = this.props;
    if (product) {
      const formattedPrice = app.formatCurrency(
        product.price,
        app.wcCurrency.code
      );
      this.setData({ formattedPrice, category: product.categories[0].name });
    }
  },
  didUpdate() {
    const { product } = this.props;
    if (product) {
      const formattedPrice = app.formatCurrency(
        product.price,
        app.wcCurrency.code
      );
      this.setData({ formattedPrice, category: product.categories[0].name });
    }
  },
  methods: {
    onAddItemToCart: function onAddItemToCart(e) {
      var onAddItemToCart = this.props.onAddItemToCart;

      if (onAddItemToCart) {
        onAddItemToCart(e);
      }
    },

    onCardClick: function onCardClick(e) {
      var onCardClick = this.props.onCardClick;

      if (onCardClick) {
        onCardClick(e);
      }
    },
  }
});
