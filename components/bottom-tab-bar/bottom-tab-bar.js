Component({
  mixins: [],
  data: {
    homePageActive: false,
    cartPageActive: false,
    profilePageActive: false
  },
  props: {
    show: false,
    cartCount: 0,
    currentPage: ''
  },
  didMount() {
    switch (this.props.currentPage) {
      case "home":
        this.setData({
          homePageActive: true,
          cartPageActive: false,
          profilePageActive: false
        });
        break;

      case "cart":
        this.setData({
          homePageActive: false,
          cartPageActive: true,
          profilePageActive: false
        });
        break;

      case "profile":
        this.setData({
          homePageActive: false,
          cartPageActive: false,
          profilePageActive: true
        });
        break;

      default:
        this.setData({
          homePageActive: false,
          cartPageActive: false,
          profilePageActive: false
        });
    }
  },
  methods: {
    goToProductList() {
      my.switchTab({
        url: "/pages/product-list/product-list"
      });
    },
    goToCartList() {
      my.switchTab({
        url: "/pages/cart/cart"
      });
    },
    goToProfile() {
      my.switchTab({
        url: "/pages/user-profile/user-profile"
      });
    }
  }
});
