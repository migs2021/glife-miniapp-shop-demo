const BOTTOM_TAB_BAR_PAGES = {
  HOME: "home",
  CART: "cart",
  PROFILE: "profile"
};

export default BOTTOM_TAB_BAR_PAGES;