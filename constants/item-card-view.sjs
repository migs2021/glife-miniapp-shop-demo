const ITEM_CARD_VIEW = {
  CART_VIEW: "cart",
  CHECKOUT_VIEW: "checkout",
  ORDER_INFO_VIEW: "order-info"
};
const isCartView = itemCardView =>{
  return itemCardView == ITEM_CARD_VIEW.CART_VIEW ? true : false;
}
  
const isCheckoutView = itemCardView => {
  return itemCardView == ITEM_CARD_VIEW.CHECKOUT_VIEW ? true : false;
};
const isOrderInfoView = itemCardView => {
  return itemCardView == ITEM_CARD_VIEW.ORDER_INFO_VIEW ? true : false;
};

const isQtyEditable = itemCardView => {
  return itemCardView == ITEM_CARD_VIEW.ORDER_INFO_VIEW || itemCardView == ITEM_CARD_VIEW.CHECKOUT_VIEW ? true : false;
};

export default { ITEM_CARD_VIEW, isCartView, isCheckoutView, isOrderInfoView };
