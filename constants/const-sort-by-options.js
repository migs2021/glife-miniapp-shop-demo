const SORT_BY_OPTIONS = [
  {
    textDisplay: "Date: Newest to Oldest",
    value: { order: "desc", orderby: "date" }
  },
  {
    textDisplay: "Date: Oldest to Newest",
    value: { order: "asc", orderby: "date" }
  },
  {
    textDisplay: "Pricing: Highest to Lowest",
    value: { order: "desc", orderby: "price" }
  },
  {
    textDisplay: "Pricing: Lowest to Highest",
    value: { order: "asc", orderby: "price" }
  }
];

export default SORT_BY_OPTIONS;
