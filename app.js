const { createSign } = require("crypto");
const moment = require("moment");
import addTimeout from "/utilities/timeout-util";

App({
  globalData: {
    user: {},
    productList: [],
    productToView: {}
  },
  cartList: [],
  userInfo: null,
  wcUserInfo: null,
  wcGeneralSettings: null,
  wcCurrency: null,
  wcCountries: null,

  wcHost: "https://woocommerce-764300-2588776.cloudwaysapps.com",
  consumerKey: "ck_fc387292672a433e9444f8031f7e06043c5cb56a",
  consumerSecret: "cs_450210fa2876b8463e7a2befb1e7dce50fcf35a4",

  host: "https://api-sit.saas.mynt.xyz",
  privateKey:
    "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAzKf8dzVDn0mEse5+N2Oyk6j4468rrwMw6fPov+QvRTQlaX+z\n6366JFbBWGBrNsNMppysCa5G8UtSw5H/ezwhqiV7ohdAC7hxoa9MGawuL2oimOPr\n0yWdDvbYMf7qKu9EsLgpktzbgVM3UOct7+qy6cdt2QCaeHmuWTTf00dF9QjiGEKd\ny7RpygTNfDpT7sHeya2WVba3zAnVeFlXw/cHGXLCflHOR4Okbl+1lmHjts6RKjPg\ncf3IS6bb8dohzM+pxGT2Xn2p1wcSuDjjnPx7J+UXPI0fS3ZRqnpsDYxGANxwwtco\nthLVzwT4i7xcPQru0BRWIXg4+x4UlLUg5rhvEwIDAQABAoIBAQCrZUyU8XDOqB2P\nfrMJ5F8cNvkqhfvJuJxwcqtMeef0xzVF44NlCF8QK/AE+csQtAMvzipueRxE3NZV\nn6IdLUDsZDPkjiA/MJtJ06uiHIcZgoOzE77euLOOYsCxLlIi3vviHHoE2x6icnTz\nNolMUnzAORsQD+dOWmnfn96L7TbzfJqPXX2RsFrNf8mF9KAJ8qnwsDp2UT5VTdSS\nh9njQXDuB1UFmsdSsvbja8KO1z0gIrirGLtmesMAaRyjC9ZozQAQBwLhX1iAikAq\ne/t/Y290ndkUETmehHnQsZIphksz0P4zbTMlohQSZUajQgYSGbzjDHwfrYbFVRT/\nAlMhHj5xAoGBAPboczZTy0yZPeJjmZU7gmyAbAgG6539QTCkwyw0+UYrKKVu0E9W\n+LGDfsYPOT2y5UPQ5Gaxy/XhLl7pFIiSpg+DuJ7jDxOfU4UXrKFkMA6TMR6InY46\nt13ILav41TLu4nG0Bxa8ozjHScihLJFNvGO0n8lD4zVP9Zpeuuylhz3fAoGBANQx\nPLg6eVRGi/xkbhUPYaUcRgv8tF+s09kN/82k5cKTOWRfPKEL+fDAYnhkTwj+KAqY\ncrKJXILgU1GvMFPeDAvLHuZfifdfqS0+DsNxMZ/SWTd3BQ+RxFVoP+HmkxnG31qw\nZKtbl+K2Gec3o68d6ZrvFTe0ocYFuBsKtLmw6Y1NAoGABnQi9suP58ulm3JBBG6O\nxSlCKZ+Mrw98kbnsxA4KafQc1t4job9DNtiaCrFBLMStS219WtXQ3/dnz+FFwSAc\nKRGnVLfYoAmtFDB/OaS1wNvGD+dAPwR34IKn9vPFFtS5pIExIj4CAizrV8wmrxGa\nF/0TpB4bJTyWzdcmCaN1+n8CgYEAhY3L+uSK8xlICH6prsW0nrhx+h9+4+0ila4z\nABs2pcYGTH+I6JBiKz7be40fFbjqOOkE4AyG1rUcnAAY+eB7UnzxIOynKLTkBV7W\n6fp30nERyFA4Njl4Co2ID8YRdhru/+SgKB6JNZ+KJDan5pN8MhEjGKGvZLS60QJn\nB60rAs0CgYBAxBiyogYkKJUV5YDuiP8A/wvbJ0b+B1Bckudkn8WJmB59yhYPaErE\nei7uxodFEWszBgphsTt7zyAKqbEYc9mO7f9+Dw5pzGNmRMUvhoEnjznemPwl+IIy\nu9hTBtKx8dRLCWXEu0jvjKnyZ6B2nh7JiKa+xqUA/XxlzavgbkOX+w==\n-----END RSA PRIVATE KEY-----",
  appId: "2170020116982966",
  merchantId: "217020000647910804623",
  clientId: "2022031715393200036607",
  
  onLoadWcCurrentCurrency() {
    return new Promise((resolve, reject) => {
      if (this.wcCurrency) {
        resolve(this.wcCurrency);
      } else {
        this.requestWcAPI("/wp-json/wc/v3/data/currencies/current")
          .then(res => {
            this.wcCurrency = res.data;
            resolve(this.wcCurrency);
          })
          .catch(error => {
            reject(error);
          });
      }
    });
  },
  basicAuth() {
    let hash = Buffer.from(
      this.consumerKey + ":" + this.consumerSecret
    ).toString("base64");
    return "Basic " + hash;
  },
  requestWcAPI(restfulPath, method = "GET", body = {}) {
    return addTimeout(
      new Promise((resolve, reject) => {
        my.request({
          url: this.wcHost + restfulPath,
          method: method,
          data: body,
          headers: { Authorization: this.basicAuth() },
          success: function(res) {
            resolve(res);
          },
          fail: function(res) {
            reject(res);
          }
        });
      })
    );
  },
  formatCurrency(amount, currency_code, minor_unit = 0) {
    const newAmount = amount * (1 / Math.pow(10, minor_unit));
    return (
      new Intl.NumberFormat("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      }).format(newAmount) +
      " " +
      currency_code
    );
  },
  onLoadCartList() {
    return new Promise(resolve => {
      my.getStorage({
        key: "cart-list",
        success: function(res) {
          resolve(res.data);
        }
      });
    });
  },
  onSetCartList(cartList) {
    return new Promise(resolve => {
      my.setStorage({
        key: "cart-list",
        data: cartList,
        success: function() {
          resolve(cartList);
        }
      });
    });
  },
  onAddToCartFn(dataValue, quantity = 1, variation = {}) {
    const addToCartProduct = dataValue.product;
    const addToCartVariation = dataValue.variation;
    const addToCartIndex = dataValue.cartKey
      ? parseInt(dataValue.cartKey)
      : null;
    const productId = addToCartVariation
      ? addToCartVariation.id
      : addToCartProduct.id;
    const attributes = addToCartProduct.attributes;
    const metaData =
      addToCartProduct.type === "variable"
        ? attributes.map(attribute => {
            return {
              key: attribute.name,
              value:
                attribute.options[
                  variation[attribute.name] ? variation[attribute.name] : 0
                ]
            };
          })
        : [];
    const cartList = this.cartList;
    const newCartList = [...cartList];
    const isProductInCart = cartList.findIndex(
      cartItem =>
        cartItem.line_item.product_id == productId &&
        cartItem.line_item.meta_data.length === metaData.length &&
        cartItem.line_item.meta_data.every(cartItemMetaData =>
          metaData.find(
            meta =>
              meta.key === cartItemMetaData.key &&
              meta.value === cartItemMetaData.value
          )
        )
    );
    return new Promise((resolve, reject) => {
      const lineItem = {
        product_id: productId,
        quantity,
        meta_data: Object.keys(variation).length > 0 ? metaData : []
      };
      const cartItem = {
        ...(addToCartVariation ? addToCartVariation : addToCartProduct),
        name: addToCartProduct.name,
        attributes,
        line_item: lineItem
      };
      if (isProductInCart >= 0) {
        if (addToCartIndex === null) {
          cartItem.line_item.quantity =
            cartList[isProductInCart].line_item.quantity + quantity;
        }
        newCartList.splice(isProductInCart, 1, cartItem);

        if (addToCartIndex !== null && addToCartIndex !== isProductInCart) {
          newCartList.splice(addToCartIndex, 1);
        }
      } else {
        if (addToCartIndex !== null) {
          newCartList.splice(addToCartIndex, 1, cartItem);
        } else {
          newCartList.push(cartItem);
        }
      }
      this.onSetCartList(newCartList).then(res => {
        resolve(res);
      });
    });
  },
  generateSignature(method, uri, requestTime, body) {
    const sign = createSign("RSA-SHA256");
    sign.update(
      `${method.toUpperCase()} ${uri}\n${
        this.clientId
      }.${requestTime}.${JSON.stringify(body)}`
    );
    sign.end();

    const signature = encodeURIComponent(sign.sign(this.privateKey, "base64"));
    return signature;
  },
  getHttpHeaders(method, uri, body) {
    const requestTime = moment()
      .add(moment.duration(-30, "seconds"))
      .format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    const signature = this.generateSignature(method, uri, requestTime, body);
    return {
      "Client-Id": this.clientId,
      Signature: `algorithm=RSA256, keyVersion=1, signature=${signature}`,
      "Content-Type": "application/json; charset=UTF-8",
      "Request-Time": requestTime
    };
  },
  getAuthCode() {
    return new Promise(resolve => {
      my.getAuthCode({
        scopes: "auth_user",
        success: res => {
          resolve(res.authCode);
        }
      });
    });
  },
  startPaymentTransaction(body = {}) {
    return new Promise((resolve, reject) => {
      my.tradePay({
        ...body,
        success: res => {
          resolve(res);
        },
        fail: res => {
          reject(res);
        }
      });
    });
  },
  requestOpenAPI(restfulPath, body) {
    return new Promise((resolve, reject) => {
      my.request({
        url: this.host + restfulPath,
        method: "POST",
        data: body,
        headers: this.getHttpHeaders("POST", restfulPath, body),
        success: function(res) {
          resolve(res.data);
        },
        fail: function(res) {
          reject(res);
        }
      });
    });
  },
  setUserInfo() {
    return addTimeout(
      new Promise((resolve, reject) => {
        if (this.userInfo) {
          resolve(this.userInfo);
        } else {
          this.getAuthCode().then(authCode => {
            if (authCode) {
              const restfulPath = "/v1/authorizations/applyToken.htm";
              const body = {
                referenceClientId: this.clientId,
                grantType: "AUTHORIZATION_CODE",
                authCode: authCode,
                extendInfo: '{"customerBelongsTo":"GCASH"}'
              };
              this.requestOpenAPI(restfulPath, body).then(res => {
                const accessToken = res.accessToken ? res.accessToken : null;
                if (accessToken) {
                  const restfulPath =
                    "/v1/customers/user/inquiryUserInfoByAccessToken.htm";
                  const body = {
                    accessToken: accessToken,
                    extendInfo: '{"customerBelongsTo":"GCASH"}'
                  };
                  this.requestOpenAPI(restfulPath, body).then(res => {
                    const userInfo = res.userInfo ? res.userInfo : null;
                    if (userInfo) {
                      const extendInfo = JSON.parse(userInfo.extendInfo);
                      const formattedUserInfo = {
                        id: userInfo.userId,
                        fullName:
                          userInfo.userName.firstName +
                          " " +
                          userInfo.userName.lastName,
                        firstName: userInfo.userName.firstName,
                        lastName: userInfo.userName.lastName,
                        avatar: "https://i.pravatar.cc/300",
                        contactType: "Mobile Phone",
                        contactNo: extendInfo.GCASH_NUMBER,
                        email: extendInfo.EMAIL_ADDRESS
                      };
                      this.userInfo = formattedUserInfo;
                      resolve(formattedUserInfo);
                    } else {
                      reject();
                    }
                  });
                } else {
                  reject();
                }
              });
            } else {
              reject();
            }
          });
        }
      })
    );
  },
  setWcUserInfo() {
    return addTimeout(
      new Promise((resolve, reject) => {
        if (!this.userInfo) {
          reject("No User Info");
        }

        if (this.wcUserInfo) {
          resolve(this.wcUserInfo);
        } else {
          const body = {
            email: this.userInfo.email
          };
          this.requestWcAPI("/wp-json/wc/v3/customers", "GET", body).then(
            res => {
              const { data } = res;
              if (data.length > 0) {
                this.wcUserInfo = data[0];
                resolve(data[0]);
              } else {
                const splittedEmail = this.userInfo.email.split("@");
                const body = {
                  email: this.userInfo.email,
                  first_name: this.userInfo.firstName,
                  last_name: this.userInfo.lastName,
                  username:
                    splittedEmail[0] +
                    this.userInfo.id.substr(this.userInfo.id.length - 4)
                };
                this.requestWcAPI(
                  "/wp-json/wc/v3/customers",
                  "POST",
                  body
                ).then(
                  res => {
                    const { data } = res;
                    this.wcUserInfo = data;
                    resolve(data);
                  },
                  res => {
                    const { data } = res;
                    reject(data);
                  }
                );
              }
            }
          );
        }
      })
    );
  },
  onLoadCountries() {
    return new Promise((resolve, reject) => {
      if (this.wcCountries) {
        resolve(this.wcCountries);
      } else {
        this.requestWcAPI("/wp-json/wc/v3/data/countries")
          .then(res => {
            this.wcCountries = res.data;
            resolve(this.wcCountries);
          })
          .catch(error => {
            reject(error);
          });
      }
    });
  },
  onSerializeQuery(params, prefix) {
    const query = Object.keys(params).map(key => {
      const value = params[key];

      if (params.constructor === Array) key = `${prefix}[]`;
      else if (params.constructor === Object)
        key = prefix ? `${prefix}[${key}]` : key;

      if (typeof value === "object") return this.onSerializeQuery(value, key);
      else return `${key}=${encodeURIComponent(value)}`;
    });

    return [].concat.apply([], query).join("&");
  }
});
